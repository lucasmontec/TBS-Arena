﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Alive : NetworkBehaviour {

    [SyncVar]
    private bool vulnerable = true;
    [SyncVar]
    private float life = 100;
    [SyncVar]
    [SerializeField]
    private float maxLife = 100;

    //Prevent dying twice on the same life.
    [SyncVar]
    private bool isDead = false;

    public delegate void OnHealthChange(float change);
    [SyncEvent]
    public event OnHealthChange EventHealthChange;

    public delegate void OnDeath();
    [SyncEvent]
    public event OnDeath EventDeathEvent;

    public delegate void OnRespawn();
    [SyncEvent]
    public event OnRespawn EventRespawnEvent;

    public float LifePercent() {
        return life / maxLife;
    }

    public bool IsAlive() {
        return !isDead;
    }

    [Server]
    public void SetVulnerable(bool val) {
        vulnerable = val;
    }

    [Server]
    public void Damage(float amt) {
        //Only accept values that do change
        //Only damage vulnerable
        if (amt > 0 && vulnerable) {
            life -= amt;
            if (life <= 0) {
                life = 0;
                
                if (!isDead) {
                    isDead = true;
                    //Throw death event
                    if (EventDeathEvent != null)
                        EventDeathEvent();
                }
            }

            //Throw health change event event
            if (EventHealthChange != null)
                EventHealthChange(amt);
        }
    }

    [Server]
    public void Heal(float amt) {
        if (amt > 0) {
            life += amt;
            if (life > maxLife)
                life = maxLife;

            if (isDead) {
                isDead = false;
                //Throw respawn event
                if (EventRespawnEvent != null)
                    EventRespawnEvent();
            }

            //Throw health change event event
            if (EventHealthChange != null)
                EventHealthChange(amt);
        }
    }
}
