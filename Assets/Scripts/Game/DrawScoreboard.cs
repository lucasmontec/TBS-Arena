﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DrawScoreboard : MonoBehaviour {

    [SerializeField]
    private GameObject scoreEntryPrefab;
    private MatchController matchController;

    private List<GameObject> entries = new List<GameObject>();

    private int currentEntry = 0;
    private int yspacing = 10;

    void Start () {
        GameObject gomc = GameObject.Find("MatchController");
        if (gomc)
            matchController = gomc.GetComponent<MatchController>();
        Redraw();
    }
	
    void OnEnable() {
        Redraw();
    }

    private void Redraw() {
        if (matchController != null) {
            //Delete old prefabs
            foreach (GameObject entry in entries) {
                Destroy(entry);
            }
            entries.Clear();

            //Add new prefabs
            currentEntry = 0;

            if (matchController.scoreboard.Count > 0) {
                foreach (KeyValuePair<string, int> score in matchController.scoreboard) {
                    var newEntry = Instantiate(scoreEntryPrefab) as GameObject;
                    newEntry.transform.SetParent(transform, false);//Set the scoreboard as parent
                    newEntry.transform.localPosition = new Vector2(0, 100 - (currentEntry * 41) - (currentEntry > 0 ? yspacing : 0));
                    currentEntry++;

                    //Populate the entry
                    newEntry.transform.Find("Player").GetComponent<Text>().text = score.Key;
                    newEntry.transform.Find("Score").GetComponent<Text>().text = score.Value + "";
                    entries.Add(newEntry);
                }
            }
        }
    }
}
