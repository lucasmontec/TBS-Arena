﻿using UnityEngine;
public class GameManager : MonoBehaviour {

    [HideInInspector]
    public Transform respawnMenu;
    [HideInInspector]
    public TBSHero localHero;

    [SerializeField]
    private GameObject scoreboard;

    void Update() {
        if (Input.GetKey("tab")){ 
            scoreboard.SetActive(true);
        }else {
            scoreboard.SetActive(false);
        }
    }

}
