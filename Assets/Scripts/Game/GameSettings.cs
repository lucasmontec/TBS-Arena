﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Stores game settings
/// </summary>
public class GameSettings : MonoBehaviour {

    /// <summary>
    /// Reference to avoid having to find this object every time we change scenes
    /// </summary>
    public static GameSettings instance;

    /// <summary>
    /// What hero the user selected on the menu.
    /// Klewr is the default hero.
    /// </summary>
    public TBSHero.Hero selectedHero = TBSHero.Hero.Klewr;
    /// <summary>
    /// Player username
    /// </summary>
    public string username = "";

    void Awake() {
        //Keep the object between scenes
        DontDestroyOnLoad(transform.gameObject);
        //Setup our reference
        instance = this;
    }

}
