﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(Alive))]
public class HealthBar : NetworkBehaviour {

    GameObject barFill;
    Slider slider;
    Alive alive;
    Text healthText;

    private float targetHealthValue = 0f;
    private float currentHealthValue = 0f;
    private float lerpAmount = 0.04f;

    void Start () {
        alive = GetComponent<Alive>();

        //If not local player, get hero health bar
        barFill = transform.Find("HealthBar/health_fill").gameObject;

        //On local player get the local health bar
        if (isLocalPlayer) {
            barFill.transform.parent.gameObject.SetActive(false);
            slider = GameObject.Find("Canvas/Slider").GetComponent<Slider>();
            healthText = GameObject.Find("Canvas/HealthText").GetComponent<Text>();
        }

        targetHealthValue = alive.LifePercent();

        alive.EventHealthChange += OnHeathChange;
    }

    void OnHeathChange(float amount) {
        targetHealthValue = alive.LifePercent();
    }

    void Update() {
        if (Mathf.Abs(currentHealthValue - targetHealthValue) > 0.001f) {
            currentHealthValue = Mathf.Lerp(currentHealthValue, targetHealthValue, lerpAmount);
            if (barFill != null && barFill.activeInHierarchy)
                barFill.GetComponent<Renderer>().material.SetFloat("_Fill", currentHealthValue);
            else {
                slider.value = currentHealthValue;
                healthText.text = "" + Mathf.CeilToInt(currentHealthValue*100);
            }
        }
    }
}
