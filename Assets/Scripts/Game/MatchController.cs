﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MatchController : NetworkBehaviour {

    public static MatchController instance;

    /// <summary>
    /// Current match time
    /// This is a 'timer'
    /// </summary>
    [SyncVar]
    public float matchTime = 0f;
    /// <summary>
    /// Total match duration (this is (roundDuration+warmup)*numberOfRounds)
    /// </summary>
    public float matchDuration = 0f;
    /// <summary>
    /// How much time each
    /// </summary>
    public float roundDuration = 0f;
    /// <summary>
    /// Number of rounds per match
    /// </summary>
    public int numberOfRounds = 5;

    public enum MatchState { WAITING_FOR_PLAYERS, WARMUP, ROUND };
    /// <summary>
    /// This is the current match state
    /// </summary>
    [SyncVar]
    public MatchState matchState = MatchState.WAITING_FOR_PLAYERS;

    public Dictionary<string, int> scoreboard = new Dictionary<string, int>();
    public List<string> users = new List<string>();

    void Awake() {
        instance = this;
    }

    void Start() {
        if (isServer) {
            StartCoroutine(refresh());
        }
    }

    [Server]
    public void RegisterPlayer(TBSHero hero) {
        //Count same users
        int amount = users.FindAll(usr => usr.Equals(hero.username)).Count;
        string sameusers = "";
        //Add a * for each time this name was already registered
        for (int i = 0; i < amount; i++) {
            sameusers += '*';
        }
        scoreboard.Add(hero.username + sameusers, 0);
        users.Add(hero.username);
        hero.username = hero.username + sameusers;

        //Send the new scoreboard
        RpcUpdateScoreboard(serializeDictionary(scoreboard));
    }

    /// <summary>
    /// Add score for the player that killed the parameter hero
    /// </summary>
    /// <param name="hero">The hero that died.</param>
    [Server]
    public void ScoreDeath(TBSHero hero) {
        scoreboard[hero.GetLastDamagingHero().username] += 1;
        /*Debug.Log(hero.username + " died for " + hero.GetLastDamagingHero().username);
        Debug.Log("Killer score: "+scoreboard[hero.GetLastDamagingHero().username]);
        Debug.Log("Killed score: "+scoreboard[hero.username]);*/

        //Send the update
        RpcUpdateScoreboard(serializeDictionary(scoreboard));
    }

    IEnumerator refresh() {
        //Send the update
        RpcUpdateScoreboard(serializeDictionary(scoreboard));
        yield return new WaitForSeconds(1f);
    }

    [ClientRpc]
    private void RpcUpdateScoreboard(string serializedScoreboard) {
        scoreboard = deserializeDictionary(serializedScoreboard);
    }

    private string serializeDictionary(Dictionary<string, int> dict) {
        string ret = "";
        foreach (KeyValuePair<string, int> entry in dict) {
            ret += entry.Key + "," + entry.Value;
            ret += "|";
        }
        return ret;
    }

    private Dictionary<string, int> deserializeDictionary(string serialized) {
        Dictionary<string, int> ret = new Dictionary<string, int>();
        string[] entries = serialized.Split('|');
        foreach(string entry in entries) {
            string[] vals = entry.Split(',');
            if(vals.Length == 2) {
                string key = vals[0];
                int value = System.Int32.Parse(vals[1]);
                ret.Add(key, value);
            }
        }
        return ret;
    }
}
