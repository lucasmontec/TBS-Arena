﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// This class sets the multiplayer name up
/// </summary>
public class NetworkName : NetworkBehaviour {

    [SyncVar(hook = "HookUpdateName")]
    private string netName = "unamed";

	void Start () {
        transform.name = "TBSHero_" + netId.ToString();
        if(hasAuthority)
            CmdUpdateName(transform.name);
    }

    public string GetNetName() {
        return netName;
    }

    [Command]
    public void CmdUpdateName(string name) {
        transform.name = name;
        this.netName = name;
    }

    public void HookUpdateName(string name) {
        netName = name;
        transform.name = name;
    }
}
