﻿using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// This class sets the multiplayer up
/// </summary>
public class NetworkSetup : NetworkBehaviour {

    //Camera to enable following
    private Camera mainCamera;
    //Disable this controller to be a network
    private Platformer2DUserControl userController;

	void Start () {
        mainCamera = Camera.main;
        userController = GetComponent<Platformer2DUserControl>();
        Camera2DFollow follow = mainCamera.transform.GetComponent<Camera2DFollow>();

        //Enable follower for local player
        if (isLocalPlayer) {
            follow.enabled = true;
            follow.target = transform;
            userController.enabled = true;
        } else {
            userController.enabled = false;
        }

        transform.name = "TBSHero_" + netId.ToString();
        CmdUpdateName(transform.name);
    }

    [Command]
    public void CmdUpdateName(string name) {
        transform.name = name;

        RpcUpdateName(name);
    }

    [ClientRpc]
    public void RpcUpdateName(string name) {
        transform.name = name;
    }
}
