using System;
using UnityEngine;
using UnityEngine.Networking;

public class PlatformerCharacter2D : NetworkBehaviour {
    [SerializeField]
    private float maxSpeed = 10f;                    // The fastest the player can travel in the x axis.
    [SerializeField]
    private float jumpForce = 400f;                  // Amount of force added when the player jumps.
    [Range(0, 1)]
    [SerializeField]
    private float crouchSpeed = .36f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
    [SerializeField]
    private bool airControl = false;                 // Whether or not a player can steer while jumping;
    [SerializeField]
    private LayerMask whatIsGround;                  // A mask determining what is ground to the character

    private Transform groundCheck;    // A position marking where to check if the player is grounded.
    const float groundedRadius = .4f; // Radius of the overlap circle to determine if grounded
    private bool grounded;            // Whether or not the player is grounded.
    private Transform ceilingCheck;   // A position marking where to check for ceilings
    const float ceilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
    private Animator animator;            // Reference to the player's animator component.
    private Rigidbody2D mRigidbody2D;
    Transform username; //The username text mesh to unflip

    public bool Crouch { get { return crouch; } }
    public bool FacingRight { get { return facingRight; } }

    //Networking
    [SyncVar(hook = "HookUpdateFlip")]
    private bool facingRight = true;  // For determining which way the player is currently facing.
    [SyncVar(hook = "HookUpdateMoving")]
    private float moving = 0f;
    [SyncVar(hook = "HookUpdateCrouch")]
    private bool crouch;

    private void Start() {
        if(isLocalPlayer)
            CmdUpdateFlip(facingRight);
    }

    private void Awake() {
        // Setting up references.
        groundCheck = transform.Find("GroundCheck");
        ceilingCheck = transform.Find("CeilingCheck");
        username = transform.Find("Name");
        animator = GetComponent<Animator>();
        mRigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate() {
        grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, whatIsGround);
        for (int i = 0; i < colliders.Length; i++) {
            if (colliders[i].gameObject != gameObject)
                grounded = true;
        }
        animator.SetBool("Ground", grounded);
    }

    public void Move(float move, bool crouch, bool jump) {
        // If crouching, check to see if the character can stand up
        if (!crouch && animator.GetBool("Crouch")) {
            // If the character has a ceiling preventing them from standing up, keep them crouching
            if (Physics2D.OverlapCircle(ceilingCheck.position, ceilingRadius, whatIsGround)) {
                crouch = true;
            }
        }

        // Set whether or not the character is crouching in the animator
        if (isLocalPlayer) {
            CmdSetCrouch(crouch);
            this.crouch = crouch;
        }
        animator.SetBool("Crouch", crouch);

        //only control the player if grounded or airControl is turned on
        if (grounded || airControl) {
            // Reduce the speed if crouching by the crouchSpeed multiplier
            move = (crouch ? move * crouchSpeed : move);

            // The Speed animator parameter is set to the absolute value of the horizontal input.
            if (isLocalPlayer) {
                moving = Mathf.Abs(move);
                CmdSetMoving(moving);
            }
            animator.SetFloat("Speed", moving);

            // Move the character
            mRigidbody2D.velocity = new Vector2(move * maxSpeed, mRigidbody2D.velocity.y);

            // If the input is moving the player right and the player is facing left...
            if (move > 0 && !facingRight) {
                // ... flip the player.
                Flip();
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (move < 0 && facingRight) {
                // ... flip the player.
                Flip();
            }
        }
        // If the player should jump...
        if (grounded && jump && animator.GetBool("Ground")) {
            // Add a vertical force to the player.
            grounded = false;
            animator.SetBool("Ground", false);
            mRigidbody2D.AddForce(new Vector2(0f, jumpForce));
        }
    }

    public void Flip() {
        facingRight = !facingRight;

        CmdUpdateFlip(facingRight);
    }

    [Command]
    public void CmdUpdateFlip(bool facing) {
        facingRight = facing;
    }

    [Command]
    public void CmdSetMoving(float value) {
        moving = value;
    }

    [Command]
    public void CmdSetCrouch(bool value) {
        crouch = value;
    }

    public void HookUpdateFlip(bool newFacingRight) {
        //ApplyTheValue
        facingRight = newFacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        Vector3 usernameScale = username.localScale;
        bool localFacingRight = theScale.x > 0;
        //If the current facing right is not correct, we need to change the facing
        if (facingRight != localFacingRight) {
            theScale.x *= -1;
            transform.localScale = theScale;
            
            usernameScale.x *= -1;
            username.localScale = usernameScale;
        }
    }

    public void HookUpdateCrouch(bool value) {
        //Apply the received crouch
        crouch = value;
        //Update the crouch state
        animator.SetBool("Crouch", value);
    }

    public void HookUpdateMoving(float newMoving) {
        //Receive the new moving
        moving = newMoving;
        //Update the animation
        animator.SetFloat("Speed", moving);
    }
}