﻿using UnityEngine;

[RequireComponent(typeof(Alive))]
public class TBSDummy : MonoBehaviour {

    //Character controller component
    PlatformerCharacter2D ccontroller;

    //Live component
    Alive alive;
    private Animator animator;
    private bool controlHead = true; //Control head movment when alive and walking
    private Transform head;
    private Rigidbody2D body2d;

    //Body collider to disable on death
    private BoxCollider2D bodyCollider;

    void Awake() {
        head = transform.Find("Head");
        body2d = GetComponent<Rigidbody2D>();
        bodyCollider = GetComponent<BoxCollider2D>();
    }

    // Use this for initialization
    void Start () {
        ccontroller = GetComponent<PlatformerCharacter2D>();
        SetControlHead(true);

        //Setup event methods
        alive = GetComponent<Alive>();
        alive.EventDeathEvent += OnDeath;
        alive.EventRespawnEvent += OnRespawn;

        //Setup animator control
        animator = GetComponent<Animator>();
        animator.SetBool("Alive", true);
    }
	
    //When hero dies, this is called
    void OnDeath() {
        animator.SetBool("Alive", false);
        animator.SetBool("Death", true);
        SetControlHead(false);
        transform.Find("Arms").gameObject.SetActive(false);
        bodyCollider.enabled = false;
    }
    void OnRespawn() {
        SetControlHead(true);
        animator.SetBool("Alive", true);
        transform.Find("Arms").gameObject.SetActive(true);
        bodyCollider.enabled = true;
    }

    // Control head movement
    void FixedUpdate() {
        //Head position
        if (controlHead) {
            float speed = body2d.velocity.x;
            float upSway = 0f;
            float sideSway = 0f;
            if (Mathf.Abs(speed) > 0.5) {
                upSway = Mathf.Sin(Time.realtimeSinceStartup * 10) * 0.15f;
                sideSway = Mathf.Sin(Time.realtimeSinceStartup * 5) * 0.12f;
            }
            if (ccontroller.Crouch) {
                head.localPosition = new Vector2(0.1f, 1.02f);
            } else {
                head.localPosition = new Vector2(0.24f + sideSway, 1.67f + upSway);
            }
        }
    }

    public void SetControlHead(bool value) {
        controlHead = value;

        Rigidbody2D body = head.GetComponent<Rigidbody2D>();
        CircleCollider2D collider = head.GetComponent<CircleCollider2D>();
        if (controlHead && body != null) {
            body.isKinematic = true;
            collider.enabled = false;
        } else {
            body.isKinematic = false;
            collider.enabled = true;
        }
    }
}
