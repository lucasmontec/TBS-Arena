﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(Alive))]
public class TBSHero : NetworkBehaviour {

    public enum Hero { Klewr, Zaza, Zuzu, Cash };

    public Sprite KlewrHead;
    public Sprite ZazaHead;
    public Sprite ZuzuHead;
    public Sprite CashHead;

    /// <summary>
    /// What hero is this hero
    /// </summary>
    [SyncVar(hook = "HookHero")]
    private Hero hero;
    /// <summary>
    /// What username should show up
    /// </summary>
    [SyncVar(hook = "HookUsername")]
    public string username;

    float force = 10;
    float dex = 10;
    const float maxDex = 100;

    [SerializeField]
    GameObject hitEffectPrefab;

    /// <summary>
    /// Idle arms
    /// </summary>
    Transform armsIdle;
    /// <summary>
    /// Punching arms
    /// </summary>
    Transform armsPunch;

    /// <summary>
    /// Punch 2d circle detector object
    /// </summary>
    Transform punchDetector;
    /// <summary>
    /// Max radius a punch hits
    /// </summary>
    private float punchDetectionRadius = 0.16f;
    /// <summary>
    /// Layer a punch hits
    /// </summary>
    [SerializeField]
    private LayerMask whatCanIPunch;

    //Character controller component
    PlatformerCharacter2D ccontroller;
    Platformer2DUserControl usercontroller;

    //Punch cooldown
    float punchTimer = 0f;
    float punchTime = 0.3f;

    //Networking
    [SyncVar]
    private bool punching = false;
    [SyncVar]
    private bool isMoving = false;

    //Live component
    Alive alive;
    private Animator animator;
    private bool controlHead = true; //Control head movment when alive and walking
    private Transform head;
    private Transform arms;
    private Rigidbody2D body2d;

    //Body collider to disable on death
    private BoxCollider2D bodyCollider;

    //Game manager for respawning
    private GameManager manager;

    /// <summary>
    /// Score system - last hero that damaged this hero
    /// This field is only set server-side.
    /// </summary>
    private TBSHero lastDamagingHero;

    void Awake() {
        head = transform.Find("Head");
        arms = transform.Find("Arms");
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        body2d = GetComponent<Rigidbody2D>();
        bodyCollider = GetComponent<BoxCollider2D>();
    }

    // Use this for initialization
    void Start () {
        armsIdle = transform.Find("Arms/arms_idle");
        armsPunch = transform.Find("Arms/arms_punch");
        punchDetector = transform.Find("PunchDetector");
        ccontroller = GetComponent<PlatformerCharacter2D>();
        usercontroller = GetComponent<Platformer2DUserControl>();
        SetControlHead(true);

        //Setup event methods
        alive = GetComponent<Alive>();
        alive.EventDeathEvent += OnDeath;
        alive.EventRespawnEvent += OnRespawn;

        //Setup animator control
        animator = GetComponent<Animator>();
        animator.SetBool("Alive", true);

        if (!isLocalPlayer) {
            body2d.isKinematic = true;
        }

        manager.respawnMenu.Find("RespawnButton").GetComponent<Button>().onClick.AddListener(Respawn);

        //Setup the hero
        if (isLocalPlayer) {
            //Set the manager reference
            manager.localHero = this;

            //Read hero from settings
            hero = GameSettings.instance.selectedHero;
            username = GameSettings.instance.username;

            SetHeroUsername();
            SelectCorrectHead();
            CmdSendHeroSetup(username, hero, true);
        }else {
            SetHeroUsername();
            SelectCorrectHead();
        }
    }

    private void HookUsername(string newusername) {
        username = newusername;
        SetHeroUsername();
    }

    private void HookHero(Hero newhero) {
        hero = newhero;
        SelectCorrectHead();
    }

    [Command]
    private void CmdSendHeroSetup(string usern, Hero hero, bool register) {
        this.hero = hero;
        username = usern;
        if(register)
            MatchController.instance.RegisterPlayer(this);
    }

    /// <summary>
    /// Setup the hero name in the text mesh
    /// </summary>
    private void SetHeroUsername() {
        transform.Find("Name").GetComponent<TextMesh>().text = username;
    }

    /// <summary>
    /// Changes the head based on the hero.
    /// Call this after head is fetched.
    /// </summary>
    private void SelectCorrectHead() {
        //Get the head renderer
        SpriteRenderer renderer = head.GetComponent<SpriteRenderer>();

        //Switch on the hero 'class'
        switch (hero) {
            case Hero.Cash:
                renderer.sprite = CashHead;
                break;
            case Hero.Klewr:
                renderer.sprite = KlewrHead;
                break;
            case Hero.Zaza:
                renderer.sprite = ZazaHead;
                break;
            case Hero.Zuzu:
                renderer.sprite = ZuzuHead;
                break;
            default:
                renderer.sprite = KlewrHead;
                break;
        }
    }

    [Command]
    void CmdSendDeath() {
        MatchController.instance.ScoreDeath(this);
    }

    //When hero dies, this is called
    void OnDeath() {
        animator.SetBool("Alive", false);
        animator.SetBool("Death", true);
        SetControlHead(false);
        transform.Find("Arms").gameObject.SetActive(false);
        ccontroller.enabled = false;
        bodyCollider.enabled = false;

        if (isLocalPlayer) {
            usercontroller.enabled = false;
            EnableRespawn();
            CmdSendDeath();
        }
    }
    void OnRespawn() {
        SetControlHead(true);
        animator.SetBool("Alive", true);
        transform.Find("Arms").gameObject.SetActive(true);
        ccontroller.enabled = true;
        bodyCollider.enabled = true;

        head.localRotation = Quaternion.identity;
        transform.position = NetworkManager.singleton.GetStartPosition().position;

        if (isLocalPlayer) {
            DisableRespawn();
            usercontroller.enabled = true;
        }
    }

    public void Respawn() {
        CmdRespawn();
    }

    [Command]
    public void CmdRespawn() {
        alive.Heal(100);
    }

    public void EnableRespawn() {
        manager.respawnMenu.gameObject.SetActive(true);
        manager.respawnMenu.GetComponent<HeroSelectorGroup>().Select(hero);
    }

    public void DisableRespawn() {
        hero = manager.respawnMenu.GetComponent<HeroSelectorGroup>().Selected().hero;
        SelectCorrectHead();
        CmdSendHeroSetup(username, hero, false);
        manager.respawnMenu.gameObject.SetActive(false);
    }

    // Control hits (weapon uses)
    void Update () {
        if (isLocalPlayer) {
            if (Input.GetButton("Fire1")) {
                if (!punching) {
                    punching = true;
                    CmdSetPunching(true);
                }

                punchTimer += Time.deltaTime;
                if (punchTimer > punchTime) {
                    punchTimer = 0f;
                    HitTest();
                }
            } else {
                if (punching) {
                    punching = false;
                    CmdSetPunching(false);
                }
            }
        }

        //Set the arms
        armsIdle.gameObject.SetActive(!punching);
        armsPunch.gameObject.SetActive(punching);
    }

    // Control head movement
    void FixedUpdate() {
        if (isLocalPlayer) {
            HeadSway();
            //Set server moving state
            CmdSetMoving(Mathf.Abs(body2d.velocity.x) > 0.5f);
        }else {
            //Reads moving from server
            HeadSway(isMoving);
        }
    }

    /// <summary>
    /// Controls head sawying while walking.
    /// Also controls head position when crouch, and arms position.
    /// </summary>
    /// <param name="ignoreSpeed"></param>
    private void HeadSway(bool ignoreSpeed = false) {
        //Head position
        if (controlHead) {
            float speed = body2d.velocity.x;
            float upSway = 0f;
            float sideSway = 0f;
            if (Mathf.Abs(speed) > 0.5f || ignoreSpeed) {
                upSway = Mathf.Sin(Time.realtimeSinceStartup * 10) * 0.15f;
                sideSway = Mathf.Sin(Time.realtimeSinceStartup * 5) * 0.12f;
            }
            if (ccontroller.Crouch) {
                head.localPosition = new Vector2(0.1f, 0.8f);
                arms.localPosition = new Vector2(-0.1f, -0.7f);
                punchDetector.localPosition = new Vector2(punchDetector.localPosition.x, 0f);
            } else {
                head.localPosition = new Vector2(0.24f + sideSway, 1.67f + upSway);
                arms.localPosition = new Vector2(arms.localPosition.x, 0);
                punchDetector.localPosition = new Vector2(punchDetector.localPosition.x, 0.56f);
            }
        }
    }

    [Command]
    public void CmdSetMoving(bool val) {
        isMoving = val;
    }

    [Command]
    public void CmdSetPunching(bool val) {
        punching = val;
    }

    public void SetControlHead(bool value) {
        controlHead = value;

        Rigidbody2D body = head.GetComponent<Rigidbody2D>();
        CircleCollider2D collider = head.GetComponent<CircleCollider2D>();
        if (controlHead && body != null) {
            body.isKinematic = true;
            collider.enabled = false;
        } else {
            body.isKinematic = false;
            collider.enabled = true;
        }
    }

    /// <summary>
    /// Tries a hit at the punch detector
    /// </summary>
    void HitTest() {
        //Check if we have a valid punch detector
        if (punchDetector != null && alive.IsAlive()) {
            //Do a circle overlap to get punched colliders
            Collider2D[] colliders = Physics2D.OverlapCircleAll(punchDetector.position, punchDetectionRadius, whatCanIPunch);
            //For each collider, hittest
            for (int i = 0; i < colliders.Length; i++) {
                GameObject hitTarget = colliders[i].gameObject;
                //If not hit self
                if (hitTarget != gameObject) {
                    //If has alive component
                    Alive talive = hitTarget.GetComponent<Alive>();
                    //If has a name component
                    NetworkName name = hitTarget.GetComponent<NetworkName>();
                    if (talive != null && talive.IsAlive() && name != null) {
                        CmdHit(name.GetNetName());
                    }
                }
            }
        }
    }

    /// <summary>
    /// Hit the alive component. Must be a valid alive component.
    /// </summary>
    /// <param name="aliveComponent"></param>
    [Command]
    void CmdHit(string target) {
        GameObject hitTarget = GameObject.Find(target);
        Alive alive = hitTarget.GetComponent<Alive>();

        if (hitEffectPrefab != null) {
            Vector3 random = new Vector2(Random.Range(-0.1f,0.1f), Random.Range(-0.1f, 0.1f));
            Quaternion rotation = Quaternion.identity;
            if (!ccontroller.FacingRight)
                rotation = Quaternion.Euler(0,0,180f);
            CmdInstantiateHitEffect(punchDetector.position + random, rotation);
        }
        alive.Damage(getForceForDex());
        hitTarget.GetComponent<TBSHero>().lastDamagingHero = this;
    }

    /// <summary>
    /// Call instantiation of effects on server
    /// </summary>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    [Command]
    private void CmdInstantiateHitEffect(Vector2 position, Quaternion rot) {
        GameObject hitEffect = GameObject.Instantiate(hitEffectPrefab, position, rot) as GameObject;
        NetworkServer.Spawn(hitEffect);
    }

    /// <summary>
    /// Gets force plus a random value that is decreased with more dex.
    /// The more dex you have, the less random and more strong are your hits.
    /// </summary>
    /// <returns></returns>
    float getForceForDex() {
        float inverseDex = maxDex - dex;
        float dexPercent = inverseDex / maxDex;
        return force*0.5f + (Random.value * dexPercent)*force*0.5f + (dex/maxDex)*force*0.7f;
    }

    /// <summary>
    /// Returns the hero that damaged this hero last.
    /// </summary>
    /// <returns></returns>
    [Server]
    public TBSHero GetLastDamagingHero() {
        return lastDamagingHero;
    }
}
