﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HeroSelector : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

    private bool isOver = false;
    private bool selected = false;
    [SerializeField]
    private HeroSelectorGroup group;

    private Color outlineColor = new Color(103f / 255f, 125f / 255f, 182f / 255f, 1f);
    private Vector2 outlineEffectDistance = new Vector2(3.2f,3.2f);

    //The hero this selector is for
    public TBSHero.Hero hero = TBSHero.Hero.Klewr;

    void Start() {
        if (group != null) {
            group.AddSelector(this);
        }
    }

    public void OnPointerEnter(PointerEventData eventData) {
        isOver = true;
    }

    public void OnPointerExit(PointerEventData eventData) {
        isOver = false;
    }

    public void OnPointerClick(PointerEventData eventData) {
        if(group != null) {
            group.Select(this);
        } else {
            selected = !selected;
        }
    }

    public void AddOutline() {
        Outline outline = transform.gameObject.GetComponent<Outline>();
        if (outline == null) {
            outline = transform.gameObject.AddComponent<Outline>();
            outline.effectColor = outlineColor;
            outline.effectDistance = outlineEffectDistance;
            outline.enabled = true;
        }
    }

    public void RemoveOutline() {
        Outline outline = transform.gameObject.GetComponent<Outline>();
        if(outline != null) {
            Destroy(outline);
        }
    }

    void Update() {
        if (isOver || selected) {
            float rockForward = 1f + 0.2f*Mathf.Sin(Time.realtimeSinceStartup*2f);
            transform.localScale = new Vector2(rockForward, rockForward);
            if(transform.rotation.z != 0f) {
                transform.rotation = Quaternion.identity;
            }
        } else {
            if(transform.localScale.x != 1f) {
                transform.localScale = new Vector2(1f, 1f);
            }
            float rockSideways = 0.2f * Mathf.Sin(Time.realtimeSinceStartup * 2f);
            transform.Rotate(0,0, rockSideways);
        }
    }

    public void SetSelected(bool val) {
        selected = val;
        if (val) {
            AddOutline();
        } else {
            RemoveOutline();
        }
    }
}
