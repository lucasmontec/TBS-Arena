﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroSelectorGroup : MonoBehaviour {

    private List<HeroSelector> selectors = new List<HeroSelector>();
    private HeroSelector selected = null;

    /// <summary>
    /// Selects by enum value
    /// </summary>
    /// <param name="hero"></param>
    public void Select(TBSHero.Hero hero) {
        HeroSelector selector = selectors.Find(s => s.hero == hero);
        if(selector != null)
            Select(selector);
    }

    /// <summary>
    /// Add a selector to manage
    /// </summary>
    /// <param name="selector"></param>
    public void AddSelector(HeroSelector selector) {
         selectors.Add(selector);
         Select(selectors[0]);
    }

    /// <summary>
    /// Select a selector
    /// </summary>
    /// <param name="selector"></param>
    public void Select(HeroSelector selector) {
        if (selectors.Contains(selector)) {
            selected = selector;
            selectors.FindAll(s => s != selected).ForEach(s => s.SetSelected(false));
            selected.SetSelected(true);

            //Set the game settings selected hero
            if(GameSettings.instance != null)
                GameSettings.instance.selectedHero = selector.hero;
        }
    }

    /// <summary>
    /// Get the currently selected hero
    /// </summary>
    /// <returns></returns>
    public HeroSelector Selected() {
        return selected;
    }
}
