﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[RequireComponent(typeof(NetworkManager))]
public class NetworkGuiHandler : MonoBehaviour {

    private NetworkManager manager;
    public InputField ipField;
    public InputField nameField;
    public Button connectButton;
    public Button hostButton;

    void Awake() {
        manager = GetComponent<NetworkManager>();
    }

    void Start() {
        if(ipField != null) {
            ipField.text = manager.networkAddress;
        }
        if(nameField != null) {
            nameField.onEndEdit.AddListener(delegate { SetSettingsUsername(nameField); });
        }
        if(connectButton != null) {
            connectButton.onClick.AddListener(ConnectClient);
        }
        if (hostButton != null) {
            hostButton.onClick.AddListener(HostServer);
        }
    }

    private void SetSettingsUsername(InputField input) {
        if(input.text.Length > 0) {
            GameSettings.instance.username = input.text;
        }
    }

    void ConnectClient() {
        //Start the client
        manager.StartClient();
        //The the network address to connect to
        manager.networkAddress = ipField.text;
    }

    void HostServer() {
        //Start client as host
        manager.StartHost();
    }
}
