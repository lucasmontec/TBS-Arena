﻿using UnityEngine;
using UnityEngine.Networking;

public class TBSNetworkManager : NetworkManager {

    // called when a new player is added for a client
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId) {
        base.OnServerAddPlayer(conn, playerControllerId);

        //PlayerController player = conn.playerControllers.Find(ctrl => ctrl.playerControllerId == playerControllerId);
        /*if (player != null) {
            Debug.Log(player.gameObject.GetComponent<TBSHero>().username);
        }*/
    }

    // called when a player is removed for a client
    public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController playerController) {
        base.OnServerRemovePlayer(conn, playerController);
    }
}
