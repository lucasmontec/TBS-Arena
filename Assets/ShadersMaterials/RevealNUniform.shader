﻿Shader "Custom/RevealNUniform" {
	Properties
	{
		_MainTex("Base (RGB), Alpha (A)", 2D) = "white" {}
	    _Fill("Fill", Range(0.0, 1.0)) = 1.0
		_MinX("MinX", Float) = 0
		_MaxX("MaxX", Float) = 1
	}

		SubShader
	{
		LOD 200

		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "Transparent"
	}

		Pass
	{
		Cull Off
		Lighting Off
		ZWrite Off
		Offset -1, -1
		Fog{ Mode Off }
		ColorMask RGB
		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

		sampler2D _MainTex;
	float4 _MainTex_ST;
	float _MinX;
	float _MaxX;
	float _Fill;

	struct appdata_t
	{
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f
	{
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
	};

	v2f vert(appdata_t v)
	{
		v2f o;
		o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
		o.texcoord = v.texcoord;
		return o;
	}

	/*half4 frag(v2f IN) : COLOR
	{
		float mdist = 0.1;
		float distTo = mdist + (_MinX + _Fill*(_MaxX - _MinX));
		float dist = IN.texcoord.x - distTo;
		float alpha = (-dist / _MaxX)*(50*mdist);
		alpha *= _MaxX-abs(_MaxX-_Fill);

		if (alpha > 1) { alpha = 1; }
		if (alpha < 0) { alpha = 0; }

		half4 colorTransparent = tex2D(_MainTex, IN.texcoord).rgba;
		colorTransparent.a = alpha*colorTransparent.a;

		return  colorTransparent;
	}*/

	half4 frag(v2f IN) : COLOR
	{
		float mdist = 0.1f;
		float distTo = mdist + (_MinX + _Fill*(_MaxX - _MinX));
		float dist = IN.texcoord.x - distTo;
		float alpha = (-dist / _MaxX)*(100 * mdist);
		alpha *= _MaxX - abs(_MaxX - _Fill);

		if (alpha > 1) { alpha = 1; }
		if (alpha < 0) { alpha = 0; }

		half4 colorTransparent = tex2D(_MainTex, IN.texcoord).rgba;
		colorTransparent.a = alpha*colorTransparent.a;

		return  colorTransparent;
	}
		ENDCG
	}
	}
}
